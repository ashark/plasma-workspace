# Spanish translations for kholidays_calendar_plugin.po package.
# Copyright (C) 2016 This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Automatically generated, 2016.
# Eloy Cuadra <ecuadra@eloihr.net>, 2016, 2019, 2021.
msgid ""
msgstr ""
"Project-Id-Version: kholidays_calendar_plugin\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-05-03 02:20+0000\n"
"PO-Revision-Date: 2021-05-27 01:03+0200\n"
"Last-Translator: Eloy Cuadra <ecuadra@eloihr.net>\n"
"Language-Team: Spanish <kde-l10n-es@kde.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 21.04.1\n"

#: HolidaysConfig.qml:89
#, kde-format
msgid "Region"
msgstr "Región"

#: HolidaysConfig.qml:93
#, kde-format
msgid "Name"
msgstr "Nombre"

#: HolidaysConfig.qml:97
#, kde-format
msgid "Description"
msgstr "Descripción"

#~ msgid "Search…"
#~ msgstr "Buscar..."

#~ msgid "Search Holiday Regions"
#~ msgstr "Buscar regiones de festividades"
